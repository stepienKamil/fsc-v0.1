﻿using FSC.Printing.Invoice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FSC.Printing
{
    public static class DocumentGeneratorFactory
    {
        public static DocumentGenerator GetGenerator(DocumentTypeEnum documentType)
        {
            switch (documentType)
            {

                case DocumentTypeEnum.Invoice:
                    {
                        return new InvoiceGenerator();
                    }
                case DocumentTypeEnum.InvoiceCorrection:
                    {
                        return new InvoiceCorrectionGenereator();
                    }
                default:
                    {
                        return null;
                    }
            }
        }
    }
}