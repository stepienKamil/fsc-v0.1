﻿using System;

namespace FSC.Printing
{
    public abstract class DocumentGenerator
    {
        public virtual void Generate()
        {
         //var asd=   GetInvoiceNumber();
        }
        public DateTime DateOfInvoice;
        public string InvoiceNumber { get; set; }
        public byte[] PDFFile;
    }
}
