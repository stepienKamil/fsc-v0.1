﻿
var TodoViewModel = function () {
    var self = this;

    self.editCheckList = ko.observable();
    self.taskLists = ko.observableArray([]);
    self.taskListItems = ko.observableArray([]);
    self.newElemntCheckList = ko.observable("");

    self.newCheckListsTitle = ko.observable("");

    var TaskList = function () {
        this.id = ko.observable();
        this.description = ko.observable();

    };
    var TaskListItem = function () {
        this.id = ko.observable("");
        this.description = ko.observable("");
        this.isCompleted = ko.observable(false);

    };
    self.animateAdd = function (element) {
        if (element.nodeType === 1) {
            var animation = 'main';
            $(element).addClass(animation).fadeIn("slow").delay(1000);

            $(element).removeClass(animation).fadeIn("slow").delay(1000);
        }
    };
    self.animateRemove = function (element) {
        if (element.nodeType === 1) {
            var animation = 'main';
            $(element).removeClass(animation).fadeOut("slow");
        }
    };
    self.RemoveCheckListItem = function (value) {
        console.log("usuwanie element " + value);
    };
    self.AddCheckListItem = function () {
        self.taskListItems.push(new TaskListItem().description(self.newElemntCheckList()).isCompleted(false).id(0));
        self.newElemntCheckList("");
    };
    self.Add = function () {
        if (self.newCheckListsTitle() !== "") {
            var data = {
                TruckId: 0,
                Description: self.newCheckListsTitle(),
            };
            var json = JSON.stringify(data)

            $.ajax({
                url: '/api/Tasks/',
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                data: json,
                success: function (results) {
                    self.taskLists.unshift(new TaskList().description(self.newCheckListsTitle()).id(results));
                    self.newCheckListsTitle("");
                }
            })
        }

    };
    self.showCheckList = function (checkList) {
        $.ajax({
            url: 'api/Tasks/Get/' + checkList.id(),
            type: 'GET',
            success: function (data) {
                self.editCheckList(new TaskList().id(data.Id).description(data.Description));
                self.taskListItems.removeAll();
                $.each(data.Items, function (index, item) {
                    self.taskListItems.push(new TaskListItem().description(item.Description).isCompleted(item.IsCompleted).id(item.Id));
                });
            },
            error: function () {
                self.errorMessage("Pokazać listy");
            }
        });
    };
    self.removeCheckList = function (checkList) {
        bootbox.dialog({
            message: "Czy na pewno usunąc listę ?", title: "Powiadomienie",
            buttons: {
                main: { label: "Anuluj", className: "btn btn-default", callback: function () { return true; } },
                success: {
                    label: "Usuń", className: "btn btn-danger", callback: function () {

                        $.ajax({
                            url: "api/Tasks/" + checkList.id(),
                            method: "DELETE"
                        })
                        .done(function () {
                            self.taskLists.remove(checkList);
                            if (checkList.id() == self.editCheckList().id()) {
                                self.editCheckList("");
                                self.showCheckList(self.taskLists()[0]);
                            }
                        })
                        .fail(function () {
                            self.errorMessage("Wystąpił błąd przy usuwaniu!")
                        });
                    }
                }
            }
        });
    };
    self.errorMessage = function (message) {
        bootbox.alert({
            title: 'Błąd',
            message: message,
            callback: function () { }
        });
    };
    self.update = function () {

        var lista = [];
        $.each(self.taskListItems(), function (i, item) {
            lista.push({ Id: item.id(), IsCompleted: item.isCompleted(), Description: item.description() })
        });
        var data = {
            Id: self.editCheckList().id(),
            Description: self.editCheckList().description(),
            Items: lista
        };

        var json = JSON.stringify(data)

        $.ajax({
            url: '/api/Tasks/' + self.editCheckList().id(),
            type: 'PUT',
            contentType: "application/json; charset=utf-8",
            data: json,
            success: function (results) {
                bootbox.dialog({
                    title: "Info",
                    message: "Zapisano zmiany!",
                    buttons: {
                        success: {
                            label: "Ok",
                            className: "btn-success",
                            callback: function () { }
                        }
                    }
                });
            },
            error: function () {
                self.errorMessage("Nie mozna zaktualizować listy!");
            }
        })

    };
    self.newCheckListsTitleAvailable = ko.computed(function () {
        return self.newCheckListsTitle().length > 0;
    })

    self.load = function () {
        $.getJSON("/api/Tasks/Get", function (response) {
            if (response != null) {
                for (var i = 0; i < response.length; i++) {
                    self.taskLists.unshift(new TaskList().id(response[i].Id).description(response[i].Description));
                }
            }
        });
    }
};
var vm = new TodoViewModel();
vm.load();
ko.applyBindings(vm);
