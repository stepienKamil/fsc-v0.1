﻿$(function () {
    $("a[data-modal]").on("click", function () {
        $("#partsModalContent").load(this.href, function () {
            $("#partsModal").modal({ keyboard: true }, "show");

            $("#ServiceItemCode").autocomplete({
                minLength: 1,
                source: function (request, response) {
                    var url = $(this.element).data("url");
                    $.getJSON(url, { search: request.term }, function (data) {

                        response(data);
                    });
                },
                appendTo: $(".modal-body"),
                select: function (event, ui) {
                    $("#ServiceItemName").val(ui.item.value);
                    $("#Rate").val(ui.item.rate);
                    //WorkOrder.calculateItems
                    $("#Quantity").select();
                },
                change: function (event, ui) {
                    if (!ui.item) {
                        WorkOrder.calculateExtendedPrice();
                        $(event.target).val("");
                    }
                }
            });
        });
        return false;
    });
});



var WorkOrder = function () {

    init: function init() {
        calculateItems();
    };
    calculateExtendedPrice: function calculateExtendedPrice() {
        var rate = $("#newOrderItem #Rate").getAmount();
        var quantity = $("#newOrderItem #Quantity").getAmount();

        if (isNaN(rate))
            rate = 0;
        if (isNaN(quantity))
            quantity = 0;

        $("#newOrderItem #ExtendedPrice").val((rate * quantity).toFixed(2).replace(".", ","));
    };
    calculateItems: function calculateItems() {
        var total = 0;
        $("#orderItem tr").each(function (item) {
            var rate = $(this).find("#Rate").getAmountFromText();
            var quantity = $(this).find("#Quantity").getAmountFromText();

            if (isNaN(rate))
                rate = 0;
            if (isNaN(quantity))
                quantity = 0;

            $(this).find("#ExtendedPrice").text((rate * quantity).toFixed(2));
        })
    };



    return {
        calculateItems: calculateItems,
        init: init,
        calculateExtendedPrice: calculateExtendedPrice

    }

}();
