﻿var filtersGenerator = (function () {

    var textInputGenerator = function (item) {
        var result = "";
        result += beginControlGroup(item.Name);
        result += " <input type='text'  name= '" + item.Name + "'  value ='" + item.Value + "'/>";
        result += endControlGroup();

        return result;
    };
    var dateInputGenerator = function (item) {
        var result = "";
        result += beginControlGroup(item.Name);
        result += " <input type='date'  name= '" + item.Name + "'/>";
        result += endControlGroup();

        return result;
    };
    var dateFromToInputGenerator = function (item) {
        var result = "";
        result += beginControlGroup(item.Name);
        result += " <input type='date'  name= '" + item.Name + "From'/>";
        result += " <input type='date'  name= '" + item.Name + "To'/>";
        result += endControlGroup();

        return result;
    };
    var dropDownListInputGenerator = function (item) {
        var result = "";
        result += beginControlGroup(item.Name);
        result += " <select name=" + item.Name + ">";
        var list = item.Value.split(",");

        $.each(list, (function (i, element) {
            result += " <option value='" + element + "'>" + element + "</option>";
        }));
        result += "</select>";
        result += endControlGroup();

        return result;
    };
    var beginControlGroup = function (Name) {
        return "<div class='filterControl'> <span class='name'>" + Name + ":&nbsp;&nbsp;</span>";
    };
    var endControlGroup = function () {
        return " <span class='close'>X</span></div>";
    };
    var makeCloseButon = function () {
        return " on";
    };

    var getGenerator = function (type, drugie) {
        switch (type) {
            case 'text':
                return textInputGenerator(drugie);
                break;
            case 'date':
                return dateInputGenerator(drugie);
                break;
            case 'dateFromTo':
                return dateFromToInputGenerator(drugie);
                break;
            case 'dropList':
                return dropDownListInputGenerator(drugie);
                break;
        }
    };
    var _init = function () {
    };

    return {
        //init: _init,
        getGenerator: getGenerator,
    }
})();