﻿getFilters();

$("#FormFilters").submit(function (e) {
    e.preventDefault();
    var formSerializeData = $("#FormFilters").serialize();
    getDataForTable(formSerializeData);
});
$("div #GetFilters").on("click", function () {
    getFilters();
});
$("div #SaveFilters").on("click", function () {
    saveFilters();
});
function getDataForTable(filters) {
    var data = {
        Filters: filters,
        Page: 1,
    };

    $.ajax({
        url: "/WorkOrder/Index/",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(data),
        success: function (result) {
            refreshTable(result);
        },
        error: function (msg) {
            errorMessage("Błąd pobierania dancyh!");
        },
    });
};
function refreshTable(table) {
    if (!table.length > 0) {
        $("#ListWorkOrders").html("<b> Brak Wyników!</b>");
    }
    else {

        $newContent = $(table);
        $("#ListWorkOrders").replaceWith($newContent);
        $newContent.effect("slide");
    }
};

function getFilters() {
    var data = {
        FilterName: "WorkOderFilters",
        Version: 1,
    };
    $.ajax({
        url: "/Filters/GetWorkOrderFilters/",
        type: "POST",
        data: data,
        success: function (result) {
            printResult(result);
        },
        error: function (msg) {
            console.log("Błąd filta " + msg);
        },
    });
};
var printResult = function (result) {
    var filterArray = new Array();
    $.each(result.Filters, function (index, item) {
        filterArray.push(filtersGenerator.getGenerator(item.Type, item));
    });
    $("#FiltersControls").html(filterArray.join(""));
    $("#FiltersControls").sortable({ handle: "span.name" });
    $("#FiltersControls").disableSelection();
    $("#FiltersControls .filterControl span.close").each(function (i, e) {
        $(e).click(function () {
            removeFilter(e);
        });
    });
};

var removeFilter = function (item) {
    list.push({ name: item.parentElement.children[1].name, html: item.parentElement.outerHTML });
    var optionList = "";
    list.forEach(function (i, el) {
        optionList += " <li><a href=#>" + i.name + "</a></li>";
    });
    $("#contenerfiltrowOption").empty().append(optionList);
    item.parentElement.remove();
};
var saveFilters = function () {
    var data = {
        Filters:  $("#FormFilters").serialize(),
        Version: 1,
    };
    $.ajax({
        url: "/Filters/SaveFilters/",
        type: "POST",
        contentType: "application/json; charset=utf-8",
       data: JSON.stringify(data),
       // data: data,
        success: function (result) {
            console.log("zapisano");
        },
        error: function (msg) {
            console.log("Błąd zapisu");
        },
    });
};
$("#contenerfiltrowOption").on("click", "li", function (e) {
    e.preventDefault();
    var elemnt = $(this).text(); //ToDo add data Attr
    $(this).remove();
    var filter = list.find(x=>x.name === elemnt);
    var index = list.indexOf(filter);
    list.splice(index, 1);

    $("#FiltersControls").append(filter.html)
    $("#FiltersControls .filterControl span.close").unbind('click');
    $("#FiltersControls .filterControl span.close").each(function (i, e) {
        $(e).click(function () {
            removeFilter(e);
        });
    });
});

var list = [];