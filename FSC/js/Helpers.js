﻿jQuery.fn.getAmount = function () {
    return parseFloat(this.val().replace(",", ".")).toFixed(2);
};
jQuery.fn.getAmountFromText = function () {
    return parseFloat(this.text().replace(",", ".")).toFixed(2);
}