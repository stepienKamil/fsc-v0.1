﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FSC.Moduls.Filter
{
    public class Filter : IFilter
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string DefaultValue { get; set; }
        public string Type { get; set; }
        public int Order { get; set; }
        public bool Visible { get; set; }
    }
}