﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FSC.Moduls.Filter
{
    public class FiltersWorkOrder
    {
        public List<IFilter> Filters { get; set; }
        public int Version { get; set; }

        public FiltersWorkOrder()
        {
            this.Filters = new List<IFilter>();
        }

        public void GetFilterList()// Make Build Pattern
        {
            this.Filters.Add(new Filter() { Name = "Od daty do", Value = "", Type = "dateFromTo", Order = 16 });
            this.Filters.Add(new Filter() { Name = "Numer faktury", Value = "", Type = "text", Order = 1 });
            this.Filters.Add(new Filter() { Name = "Data wystawienia faktury", Value = "", Type = "date", Order = 3 });
            this.Filters.Add(new TextFilter("Filtr testowy", 7) { Value = "test" });
            //this.Filters.Add(new Filter() { Name = "dropList", Value = "testt2,dwa", Type = "dropList", Order = 10 });
            this.Filters.Add(new DropDowsnFilter("Usługi", 13, new List<string> {"Wszystkie", "JS", "Css" }));

        }
    }
}