﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FSC.Moduls.Filter
{
    public interface IFilter
    {
        string Name { get; set; }
        string Value { get; set; }
        string DefaultValue { get; set; }
        string Type { get; set; }
        int Order { get; set; }
        bool Visible { get; set; }
    }
}