﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FSC.Moduls.Filter
{
    public class TextFilter : IFilter
    {
        public TextFilter(string Name, int Order)
        {
            this.Name = Name;
            this.DefaultValue = "";
            this.Order = Order;
            this.Type = "text";
            //this.Visible
            //this.Value
        }
        public string Name { get; set; }
        public string Value { get; set; }
        public string DefaultValue { get; set; }
        public string Type { get; set; }
        public int Order { get; set; }
        public bool Visible { get; set; }
    }
}