﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FSC.DataLeyer;

namespace FSC.Controllers
{
    [Authorize]
    public class CustomersController : Controller
    {
        private ApplicationDbContext applicationDB = null;

        public CustomersController()
        {
            applicationDB = new ApplicationDbContext();
        }

        // GET: Customers/Index
        public async Task<ActionResult> Index()
        {
            return View(await applicationDB.Customers.ToListAsync());
        }

        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Customer customer = await applicationDB.Customers.FindAsync(id);
            if (customer == null)
                return HttpNotFound();
            return View(customer);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Customer customer)
        {
            if (ModelState.IsValid)
            {
                applicationDB.Customers.Add(customer);
                await applicationDB.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(customer);
        }

        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Customer customer = await applicationDB.Customers.FindAsync(id);
            if (customer == null)
                return HttpNotFound();
            return View(customer);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Customer customer)
        {
            if (ModelState.IsValid)
            {
                applicationDB.Entry(customer).State = EntityState.Modified;
                await applicationDB.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(customer);
        }

        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Customer customer = await applicationDB.Customers.FindAsync(id);
            if (customer == null)
                return HttpNotFound();
            return View(customer);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Customer customer = await applicationDB.Customers.FindAsync(id);
            applicationDB.Customers.Remove(customer);
            await applicationDB.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> GetCustomers(string name)
        {
            var customers = applicationDB.Customers.Where(x => x.CompanyName.Contains(name)).ToListAsync();
            return Json(new { success = true, data = await customers });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                applicationDB.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
