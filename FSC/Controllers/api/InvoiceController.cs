﻿using AutoMapper;
using FSC.DataLeyer;
using FSC.Printing;
using FSC.ViewModels.api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;


namespace FSC.Controllers.api
{
    public class InvoiceController : ApiController
    {
        private ApplicationDbContext applicationDB = null;
        public InvoiceController()
        {
            applicationDB = new ApplicationDbContext();
        }

        [HttpGet]
        [Route("api/Invoice/Show/{Id}")]
        public IHttpActionResult Show(int Id)
        {
            return Ok();
        }

        [HttpPost]
        public IHttpActionResult Post(MakeInvoiceResult mir)
        {
            if (mir == null)
                return NotFound();
            var wo = applicationDB.WorkOrders.FirstOrDefault(x => x.WorkOrderId == mir.WorkOrderId);
            if (wo == null)
                return NotFound();
            wo.Invoiced = true;
            var invoice = DocumentGeneratorFactory.GetGenerator(DocumentTypeEnum.Invoice);
            invoice.Generate();

            var id = new InvoiceDocument();
            id.WorkOrderId = mir.WorkOrderId;
            id.CustomerId = wo.CustomerId;
            id.InvoiceNmuber = "Faktura " + invoice.InvoiceNumber;
            id.DateOfInvoice = invoice.DateOfInvoice;
            id.FileName = "Faktura " + invoice.InvoiceNumber + ".pdf";
            id.File = invoice.PDFFile;
            applicationDB.InvoiceDocuments.Add(id);
            applicationDB.SaveChanges();

            mir.InvoiceNmuber = id.InvoiceNmuber;
            var url = Url.Link("Default", new { Controller = "api/Invoice", Action = "Show", id = id.Id });
            mir.Url = url.ToString();
            return Ok(mir);
        }
    }
}

