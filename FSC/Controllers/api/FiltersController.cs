﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using FSC.Moduls.Filter;
using FSC.ViewModels.api;

namespace FSC.Controllers.api
{
    public class FiltersController : ApiController
    {
        [HttpPost]
        [Route("Filters/GetWorkOrderFilters")]
        public FiltersWorkOrder GetWorkOrderFilters(FiltersWorkOrder FilterName)
        {
            //todo if exist get from db else generate 
            var filters = new FiltersWorkOrder();
            filters.GetFilterList();
            filters.Version = 2;

            return filters;
        }

        [HttpPost]
        [Route("Filters/SaveFilters")]
        public FiltersWorkOrder SaveFilters(FiltersWorkOrderToSaveVM filters)
        {
            //todo save to db
            return new FiltersWorkOrder();
        }
    }
}