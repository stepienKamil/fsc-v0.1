﻿using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using System.Web.Http;
using FSC.DataLeyer;
using FSC.ViewModels;
using System.Data.Entity;
using System;

namespace FSC.Controllers.api
{
    //[RoutePrefix("Tasks")] 
    [Authorize]
    public class TasksController : ApiController
    {
        private ApplicationDbContext applicationDB = null;

        public TasksController()
        {
            applicationDB = new ApplicationDbContext();
        }

        [HttpGet]
        //[Route("api/Tasks/Get")]
        public IEnumerable<CheckList> Get()
        {
            var data = applicationDB.CheckLists.Where(x => x.ParentId == 0).ToList();
            return data;
        }

        [Route("api/Tasks/Get/{id:int:max(10000)}")]
        public IHttpActionResult Get(int id)
        {
            if (id == 0)
                return BadRequest();

            var checkList = applicationDB.CheckLists.First(x => x.Id == id);
            if (checkList == null)
                return NotFound();
            var result = Mapper.Map<CheckListDisplayVM>(checkList);
            var checkListItems = applicationDB.CheckLists.Where(x => x.ParentId == checkList.Id).ToList();
            result.Items = Mapper.Map<List<CheckListItem>>(checkListItems);

            return Ok(result);
        }

        [HttpPost]
        public IHttpActionResult Create([FromBody]CheckList value)
        {
            if (string.IsNullOrWhiteSpace(value.Description))
                return BadRequest();
            value.UserId = User.Identity.GetUserId();
            applicationDB.CheckLists.Add(value);
            applicationDB.SaveChanges();

            return Ok(value.Id);
        }

        [HttpPut]
        public IHttpActionResult Update(int id, [FromBody]CheckListDisplayVM value)
        {
            if (id == 0)
                return BadRequest();

            var checkList = Mapper.Map<CheckList>(value);
            var checkListItems = Mapper.Map<List<CheckList>>(value.Items);

            checkListItems.ForEach(x => x.ParentId = checkList.Id);
            checkListItems.Add(checkList);
            checkListItems.ForEach(x => x.UserId = User.Identity.GetUserId());
            checkListItems.ForEach(x =>
            {
                if (x.Id == 0)
                    applicationDB.Entry(x).State = EntityState.Added;
                else
                    applicationDB.Entry(x).State = EntityState.Modified;
            });
            applicationDB.SaveChanges();

            return Ok();
        }
        [HttpDelete]

        public IHttpActionResult Delete(int id)
        {
            if (id == 0)
                return BadRequest();
            var item = applicationDB.CheckLists.First(x => x.Id == id);
            if (item == null)
                return NotFound();

            applicationDB.CheckLists.Remove(item);
            var items = applicationDB.CheckLists.Where(x => x.ParentId == id);
            applicationDB.CheckLists.RemoveRange(items);

            applicationDB.SaveChanges();

            return Ok();
        }
        
    }
}