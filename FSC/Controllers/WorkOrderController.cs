﻿using FSC.DataLeyer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using FSC.ViewModels;
using AutoMapper;

namespace FSC.Controllers
{
    [Authorize]
    public class WorkOrderController : Controller
    {
        private ApplicationDbContext applicationDB = null;
        public WorkOrderController(ApplicationDbContext con)
        {
            applicationDB = con;
        }

        // GET: WorkOrder/Index
        public async Task<ActionResult> Index()
        {
            var workList = applicationDB.WorkOrders.Include(x=>x.InvoiceDocuments).ToList();
            IEnumerable<WorkOrderListVM> wo = Mapper.Map<List<WorkOrderListVM>>(workList);
            if (Request.IsAjaxRequest())
                return PartialView("_ListWorkOrders", wo);

            return View(wo);
        }
        [HttpPost]
        public async Task<ActionResult> Index(WorkOrderSerachFilterVM vm = null)
        {
            var workList = applicationDB.WorkOrders.Include(x => x.InvoiceDocuments).ToList();
            IEnumerable<WorkOrderListVM> wo = Mapper.Map<List<WorkOrderListVM>>(workList);
            if (Request.IsAjaxRequest())
                return PartialView("_ListWorkOrders", wo);

            return View(wo);
        }


        public ActionResult Create()
        {
            var vm = new CreateWorkOrderVM();
            vm.Cutomers = getCutomersSelectList();
            vm.OrderDateTime = DateTime.Now;
            return View(vm);
        }

        private SelectList getCutomersSelectList()
        {
            var list = applicationDB.Customers
                            .Select(x => new SelectListItem()
                            {
                                Text = x.CompanyName,
                                Value = x.CustomerId.ToString()
                            }).ToList();
            return new SelectList(list, "Value", "Text");
        }

        [HttpPost]
        public ActionResult Create(CreateWorkOrderVM vm)
        {
            WorkOrder wo = Mapper.Map<WorkOrder>(vm);
            if (ModelState.IsValid)
            {
                wo.UserId = User.Identity.GetUserId();
                applicationDB.WorkOrders.Add(wo);
                applicationDB.SaveChanges();
                return RedirectToAction("Edit", new { controller = "WorkOrder", action = "Edit", Id = wo.WorkOrderId });
            }
            vm.Cutomers = getCutomersSelectList();
            return View();
        }

        public async Task<ActionResult> Edit(int? Id)
        {
            if (Id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            WorkOrder wo = await applicationDB.WorkOrders.FindAsync(Id);
            if (wo == null)
                return HttpNotFound();
            //  ViewBag.CustomerId = new SelectList(baza.Customers, "CustomerI", "AccountNumber", wo.CustomerId);
            return View(wo);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(WorkOrder wo)
        {
            if (ModelState.IsValid)
            {
                wo.UserId = User.Identity.GetUserId();
                applicationDB.Entry(wo).State = EntityState.Modified;
                await applicationDB.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        public ActionResult ListOrderitem(int workOrderId)
        {
            var workOrderItem = applicationDB.WorkOrders.SingleOrDefault(x => x.WorkOrderId == workOrderId).OrderItems;
            ViewBag.WorkOrder = workOrderId;
            return PartialView("_ListOrderitem", workOrderItem);
        }

        public ActionResult CreateOrderItem(int workOrderId)
        {
            var orderItem = new OrderItem();
            orderItem.WorkOrderId = workOrderId;
            return PartialView("_CreateOrderItem", orderItem);
        }

        [HttpPost]
        public async Task<ActionResult> CreateOrderItem(OrderItem oi)
        {
            if (ModelState.IsValid)
            {
                if (oi.OrderItemId == 0)
                {
                    applicationDB.Entry(oi).State = EntityState.Added;
                    applicationDB.OrderItems.Add(oi);
                }
                else
                    applicationDB.Entry(oi).State = EntityState.Modified;

                await applicationDB.SaveChangesAsync();
            }
            return RedirectToAction("Edit", new { id = oi.WorkOrderId });
        }
        public async Task<ActionResult> EditOrderItem(int orderItemId)
        {
            var orderItem = await applicationDB.OrderItems.FirstOrDefaultAsync(x => x.OrderItemId == orderItemId);
            return PartialView("_CreateOrderItem", orderItem);
        }

        public async Task<ActionResult> DeleteOrderItem(int orderItemId)
        {
            var oi = await applicationDB.OrderItems.FirstOrDefaultAsync(x => x.OrderItemId == orderItemId);
            return PartialView("_DeleteOrderItem", oi);
        }
        public async Task<ActionResult> DeleteOrderItemConfirmed(int orderItemId, int workOrderId)
        {
            var oi = applicationDB.OrderItems.FirstOrDefault(x => x.OrderItemId == orderItemId);
            applicationDB.Entry(oi).State = EntityState.Deleted;
            applicationDB.OrderItems.Remove(oi);
            await applicationDB.SaveChangesAsync();

            return RedirectToAction("Edit", new { id = workOrderId });
        }

        public JsonResult GetServiceItemForAutocomplete(string search)
        {
            if (string.IsNullOrWhiteSpace(search))
                return null;
            var serviceItem = applicationDB.ServiceItems.Where(si =>
                si.ServiceItemCode.Contains(search) ||
                si.ServiceItemName.Contains(search)).ToList();

            return Json(serviceItem.Select(si => new
            {
                id = si.ServiceItemCode,
                value = si.ServiceItemName,
                label = string.Format("{0}: {1}", si.ServiceItemCode, si.ServiceItemName),
                rate = si.Rate
            }), JsonRequestBehavior.AllowGet);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                applicationDB.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}