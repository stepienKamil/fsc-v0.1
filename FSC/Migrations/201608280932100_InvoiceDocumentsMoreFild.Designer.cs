// <auto-generated />
namespace FSC.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class InvoiceDocumentsMoreFild : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(InvoiceDocumentsMoreFild));
        
        string IMigrationMetadata.Id
        {
            get { return "201608280932100_InvoiceDocuments5"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
