namespace FSC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InvoiceDocuments : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.InvoiceDocuments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InvoiceNmuber = c.String(),
                        WorkOrderId = c.Int(nullable: false),
                        CustomerId = c.Int(nullable: false),
                        DateOfInvoice = c.DateTime(nullable: false),
                        FileName = c.String(),
                        File = c.Binary(),
    
        })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.WorkOrders", "Invoiced", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.WorkOrders", "Invoiced");
            DropTable("dbo.InvoiceDocuments");
        }
    }
}
