namespace FSC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddtableforInvoiceGenerator : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.InvoiceCounters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DocumentType = c.String(),
                        Month = c.String(),
                        Year = c.String(),
                        Counter = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.InvoiceTemplates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DocumentType = c.String(),
                        Template = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.InvoiceTemplates");
            DropTable("dbo.InvoiceCounters");
        }
    }
}
