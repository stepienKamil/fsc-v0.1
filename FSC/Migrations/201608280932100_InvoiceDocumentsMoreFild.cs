namespace FSC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InvoiceDocumentsMoreFild : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InvoiceDocuments", "FileName", c => c.String());
            AddColumn("dbo.InvoiceDocuments", "File", c => c.Binary());
            CreateIndex("dbo.InvoiceDocuments", "WorkOrderId");
            AddForeignKey("dbo.InvoiceDocuments", "WorkOrderId", "dbo.WorkOrders", "WorkOrderId", cascadeDelete: true);
            DropColumn("dbo.InvoiceDocuments", "PDF");
        }
        
        public override void Down()
        {
            AddColumn("dbo.InvoiceDocuments", "PDF", c => c.Binary());
            DropForeignKey("dbo.InvoiceDocuments", "WorkOrderId", "dbo.WorkOrders");
            DropIndex("dbo.InvoiceDocuments", new[] { "WorkOrderId" });
            DropColumn("dbo.InvoiceDocuments", "File");
            DropColumn("dbo.InvoiceDocuments", "FileName");
        }
    }
}
