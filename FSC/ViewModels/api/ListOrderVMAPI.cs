﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FSC.ViewModels.api
{
    public class ListOrderVMAPI
    {
        public int WorkOrderId { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get;  set; }
        public DateTime OrderDateTime { get; set; }
        public string Description { get; set; }
        public decimal Total { get; set; }
        public bool Invoiced { get; set; }
        public List<WorkOrderListInvoiceDocuments> InvoiceDocuments { get; set; }  
    }
    public class WorkOrderListInvoiceDocumentsVMAPI
    {
        public string InvoiceNmuber { get; set; }
        public string Url { get; set; }
    }
}