﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FSC.ViewModels.api
{
    public class MakeInvoiceResult
    {
        public string Url { get; set; }
        public string InvoiceNmuber { get; set; }
        public int WorkOrderId { get; set; }
    }
}