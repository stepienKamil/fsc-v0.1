﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FSC.ViewModels.api
{
    public class FiltersWorkOrderToSaveVM
    {
        public string Filters { get; set; }
        public int Version { get; set; }
    }
}