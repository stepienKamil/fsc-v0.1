﻿using FSC.DataLeyer;
using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Collections.Generic;

namespace FSC.ViewModels
{
    public class WorkOrderListVM
    {
        public int WorkOrderId { get; set; }
        public int CustomerId { get; set; }
        public virtual Customer Customer { get; set; }
        [Display(Name = "Data zamówienia")]
        public DateTime OrderDateTime { get; set; }
        [Display(Name = "Opis")]
        public string Description { get; set; }
        [Display(Name = "Suma")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c}")]
        public decimal Total { get; set; }
        public bool Invoiced { get; set; }
        public List<WorkOrderListInvoiceDocuments> InvoiceDocuments { get; set; }
    }
    public class WorkOrderListInvoiceDocuments
    {
        public string InvoiceNmuber { get; set; }
        public string Url { get; set; }
    }
}