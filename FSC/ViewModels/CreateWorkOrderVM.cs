﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FSC.ViewModels
{
    public class CreateWorkOrderVM
    {
        public int WorkOrderId { get; set; }
        public int CustomerId { get; set; }
        public SelectList Cutomers { get;  set; }
        public DateTime OrderDateTime { get;  set; }
        [Display(Name = "Opis")]
        public string Description { get; set; }
    }
}