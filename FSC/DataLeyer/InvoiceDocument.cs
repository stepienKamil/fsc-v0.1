﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FSC.DataLeyer
{
    public class InvoiceDocument
    {
        
        public int Id { get; set; }
        public string InvoiceNmuber { get; set; }
      
        public int WorkOrderId { get; set; }
        [ForeignKey("WorkOrderId")]
        public virtual WorkOrder WorkOrder { get; set; }
        public int CustomerId { get; set; }
        public DateTime DateOfInvoice { get; set; }
        public string FileName { get; set; }
        public byte[] File { get; set; }

    }
}