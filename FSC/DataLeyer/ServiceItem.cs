﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FSC.DataLeyer
{
    public class ServiceItem
    {
        public int ServiceItemId { get; set; }
        [Required]
        public string ServiceItemCode { get; set; }
        [Required]
        public string ServiceItemName { get; set; }
        public decimal Rate { get; set; }
    }
}