﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FSC.DataLeyer
{
    public class OrderItem
    {
        public int OrderItemId { get; set; }
        [Required]
        public int WorkOrderId { get; set; }
        public virtual WorkOrder WorkOrder { get; set; }
        [Required]
        [Display(Name = "Kod usługi")]
        public string ServiceItemCode { get; set; }
        [Required]
        [Display(Name = "Nazwa usługi")]
        public string ServiceItemName { get; set; }
        [Display(Name = "Ilość")]
        public decimal Quantity { get; set; }
        [Display(Name = "Stawka")]
        public decimal Rate { get; set; }
        [Display(Name = "Wartość")]
        //[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [NotMapped]
        public virtual decimal ExtendedPrice { get; set; }
    }
}