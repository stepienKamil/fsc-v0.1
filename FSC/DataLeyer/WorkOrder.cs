﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FSC.DataLeyer
{
    public class WorkOrder
    {
        public int WorkOrderId { get; set; }
        public int CustomerId { get; set; }
        public virtual Customer Customer { get; set; }
        [Display(Name = "Data zamówienia")]
        public DateTime OrderDateTime { get; set; }
        public virtual List<OrderItem> OrderItems { get; set; }
        [Display(Name = "Opis")]
        public string Description { get; set; }
        //public WorkOrderStatus WorkOrderStatus { get; set; }
        [Display(Name = "Suma")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c}")]
        // [NotMapped]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public decimal Total { get; set; }
        public bool Invoiced { get; set; }
        public virtual List<InvoiceDocument> InvoiceDocuments { get; set; }
        public ApplicationUser User { get; set; }
        public string UserId { get; set; }

        // public OrderItem OrderItem { get; set; }


    }
    
}