﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;

namespace FSC.DataLeyer
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            this.Configuration.LazyLoadingEnabled = true;
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<WorkOrder> WorkOrders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<ServiceItem> ServiceItems { get; set; }
        public DbSet<CheckList> CheckLists { get; set; }

        public DbSet<InvoiceTemplate> InvoiceTemplates { get; set; }
        public DbSet<InvoiceCounter> InvoiceCounters { get; set; }
        public DbSet<InvoiceDocument> InvoiceDocuments { get; set; }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

    }
}