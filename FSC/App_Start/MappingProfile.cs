﻿using AutoMapper;
using FSC.DataLeyer;
using FSC.ViewModels;
using FSC.ViewModels.api;

namespace FSC.App_Start
{
    public class MappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMissingTypeMaps = true;
                //cfg.CreateMap<Source, Dest>()
                //   .ForMember(d => d.UserName,
                //       opt => opt.MapFrom(src => userName)
                //   );
                cfg.AddProfile<WebApiProfile>();
                cfg.CreateMap<CreateWorkOrderVM, WorkOrder>().ReverseMap();
                cfg.CreateMap<WorkOrder, WorkOrderListVM>()
                .ForMember(x => x.InvoiceDocuments, opt => opt.MapFrom(src => src.InvoiceDocuments));
                cfg.CreateMap<InvoiceDocument, WorkOrderListInvoiceDocuments>().ReverseMap();
             
            });
        }
    }

    public class WebApiProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<CheckList, CheckListDisplayVM>().ForMember(d => d.Description,
                       opt => opt.MapFrom(src => src.Description)).ReverseMap();

            CreateMap<CheckList, CheckListItem>().ReverseMap();
            CreateMap<WorkOrder, ListOrderVMAPI>()
               .ForMember(d => d.CustomerName, opt => opt.MapFrom(src => src.Customer.CompanyName))
               .ReverseMap();

        }
    }
}